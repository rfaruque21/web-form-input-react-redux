import React from "react"
import configureStore from "redux-mock-store"

import { fireEvent, reduxRender as render } from "../../utils/test-utils"
import UserInput from "./UserInput"

const mockStore = configureStore([])

describe("UserInput", () => {
  it("should disable the button when form is invalid (no name in name field)", () => {
    const store = mockStore({
      user: { name: "", email: "", role: "", password: "" }
    })
    const { getByTestId } = render(<UserInput />, { store })

    expect(getByTestId("button--user")).toBeDisabled()

    fireEvent.change(getByTestId("input--name"), {
      target: { value: "" }
    })

    fireEvent.change(getByTestId("input--role"), {
      target: { value: "" }
    })

    fireEvent.change(getByTestId("input--email"), {
      target: { value: "optout@optout.com" }
    })

    fireEvent.change(getByTestId("input--password"), {
      target: { value: "Abc123ghyu" }
    })

    expect(getByTestId("button--user")).toBeDisabled()
  })

  it("should disable the button when form is invalid (invalid email field)", () => {
    const store = mockStore({
      user: { name: "", email: "", role: "", password: "" }
    })
    const { getByTestId } = render(<UserInput />, { store })

    expect(getByTestId("button--user")).toBeDisabled()

    fireEvent.change(getByTestId("input--name"), {
      target: { value: "I am a name" }
    })

    fireEvent.change(getByTestId("input--role"), {
      target: { value: "" }
    })

    fireEvent.change(getByTestId("input--email"), {
      target: { value: "optout@optout." }
    })

    fireEvent.change(getByTestId("input--password"), {
      target: { value: "Abc123ghyu" }
    })

    expect(getByTestId("button--user")).toBeDisabled()
  })

  it("should disable the button when form is invalid (invalid password field)", () => {
    const store = mockStore({
      user: { name: "", email: "", role: "", password: "" }
    })
    const { getByTestId } = render(<UserInput />, { store })

    expect(getByTestId("button--user")).toBeDisabled()

    fireEvent.change(getByTestId("input--name"), {
      target: { value: "I am a name" }
    })

    fireEvent.change(getByTestId("input--role"), {
      target: { value: "" }
    })

    fireEvent.change(getByTestId("input--email"), {
      target: { value: "optout@optout.co.uk" }
    })

    fireEvent.change(getByTestId("input--password"), {
      target: { value: "Aaaaabbb" }
    })

    expect(getByTestId("button--user")).toBeDisabled()
  })

  it("should allow the button to the enabled when the form is valid", () => {
    const store = mockStore({
      user: { name: "", email: "", role: "", password: "" }
    })
    const { getByTestId } = render(<UserInput />, { store })

    expect(getByTestId("button--user")).toBeDisabled()

    fireEvent.change(getByTestId("input--name"), {
      target: { value: "I am a name" }
    })

    fireEvent.change(getByTestId("input--role"), {
      target: { value: "" }
    })

    fireEvent.change(getByTestId("input--email"), {
      target: { value: "optout@optout.com" }
    })

    fireEvent.change(getByTestId("input--password"), {
      target: { value: "Abc123ghyu" }
    })

    expect(getByTestId("button--user")).not.toBeDisabled()

    fireEvent.click(getByTestId("button--user"))

    expect(store.getActions()).toStrictEqual([
      {
        currentPage: "USER",
        type: "SET_CURRENT_PAGE"
      },
      {
        name: "I am a name",
        type: "SET_NAME"
      },
      {
        role: "",
        type: "SET_ROLE"
      },
      {
        email: "optout@optout.com",
        type: "SET_EMAIL"
      },
      {
        password: "Abc123ghyu",
        type: "SET_PASSWORD"
      }
    ])
  })
})
