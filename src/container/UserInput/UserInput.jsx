import React, { useEffect, useState } from "react"
import { useDispatch } from "react-redux"
import { useHistory } from "react-router-dom"

import useInput from "../../hooks/useInput"

import Container from "../../components/UIElements/Container/Container"
import Input from "../../components/UIElements/Form/Input/Input"
import Button from "../../components/UIElements/Form/Button/Button"

import { TAB_ID } from "../../components/TabbedStatusBar/config/config"
import { SET_CURRENT_PAGE } from "../../store/actions/config/actions"
import {
  checkValidEmail,
  checkValidPassword,
  notEmpty
} from "./config/validators"
import {
  SET_EMAIL,
  SET_NAME,
  SET_PASSWORD,
  SET_ROLE
} from "../../store/actions/user/actions"

import classes from "./UserInput.module.scss"

const UserInput = () => {
  const history = useHistory()
  const dispatch = useDispatch()

  const {
    value: nameValue,
    onChangeHandler: nameOnChangeHandler,
    onBlurHandler: nameOnBlurHandler,
    isValid: nameIsValid,
    error: nameError
  } = useInput(notEmpty)

  const [roleValue, setRoleValue] = useState("")

  const {
    value: emailValue,
    onChangeHandler: emailOnChangeHandler,
    onBlurHandler: emailOnBlurHandler,
    isValid: isEmailValid,
    error: emailError
  } = useInput(checkValidEmail)

  const {
    value: passwordValue,
    onChangeHandler: passwordOnChangeHandler,
    onBlurHandler: passwordOnBlurHandler,
    isValid: isPasswordValid,
    error: passwordError
  } = useInput(checkValidPassword)

  const onButtonClickHandler = () => {
    dispatch({ type: SET_NAME, name: nameValue })
    dispatch({ type: SET_ROLE, role: roleValue })
    dispatch({ type: SET_EMAIL, email: emailValue })
    dispatch({ type: SET_PASSWORD, password: passwordValue })

    history.push("/comms")
  }

  useEffect(() => {
    dispatch({ type: SET_CURRENT_PAGE, currentPage: TAB_ID.USER })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const isTheFormValid = nameIsValid && isEmailValid && isPasswordValid

  return (
    <Container>
      <h2>Please enter your details into the form below</h2>
      <Input
        id="name"
        labelText="name"
        required={true}
        onChange={nameOnChangeHandler}
        onBlur={nameOnBlurHandler}
        errorText="Please enter your name"
        error={nameError}
        value={nameValue}
      />

      <Input
        id="role"
        labelText="role"
        onChange={setRoleValue}
        value={roleValue}
      />

      <Input
        id="email"
        labelText="email"
        required={true}
        onChange={emailOnChangeHandler}
        onBlur={emailOnBlurHandler}
        errorText="Please enter a valid email address"
        error={emailError}
        value={emailValue}
      />

      <Input
        id="password"
        labelText="password"
        type="password"
        required={true}
        onChange={passwordOnChangeHandler}
        onBlur={passwordOnBlurHandler}
        errorText="Please enter a password that contains at least one number, one lowercase, one uppercase character and is a minimum of 10 characters long"
        error={passwordError}
        value={passwordValue}
      />

      <div className={classes.UserInput__CTA}>
        <Button
          id="user"
          disable={!isTheFormValid}
          onClick={onButtonClickHandler}
        />
      </div>
    </Container>
  )
}

export default UserInput
