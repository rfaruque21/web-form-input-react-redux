export const notEmpty = (value) => value.trim() !== ""

export const minimumCharacter = (value, min) => value.length >= min

export const checkValidEmail = (email) => {
  const expressionToCheck = new RegExp(
    "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$"
  )

  return expressionToCheck.test(email) && notEmpty(email)
}

export const checkValidPassword = (password) => {
  const expressionToCheck = new RegExp("(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])")

  return expressionToCheck.test(password) && minimumCharacter(password, 10)
}
