import {
  checkValidEmail,
  checkValidPassword,
  minimumCharacter,
  notEmpty
} from "./validators"

describe("notEmpty", () => {
  it("should return true if the input string is not empty", () => {
    const valueUnderTest = notEmpty("hello")

    expect(valueUnderTest).toBe(true)
  })

  it("should return false if the input string is not empty", () => {
    const valueUnderTest = notEmpty("")

    expect(valueUnderTest).toBe(false)
  })
})

describe("minimumCharacter", () => {
  it("should return true if the minimum number of characters has been met", () => {
    const valueUnderTest = minimumCharacter("string", 3)

    expect(valueUnderTest).toBe(true)
  })

  it("should return false if the minimum number of characters has not been met", () => {
    const valueUnderTest = minimumCharacter("string", 9)

    expect(valueUnderTest).toBe(false)
  })
})

describe("checkValidEmail", () => {
  it("should return true for a valid email", () => {
    const valueUnderTest = checkValidEmail("iamavalidemail@hotmail.co.uk")

    expect(valueUnderTest).toBe(true)
  })

  it("should return false for an invalid email", () => {
    const valueUnderTest = checkValidEmail("dfs@ko.")

    expect(valueUnderTest).toBe(false)
  })
})

describe("checkValidPassword", () => {
  it("should return true for a valid password", () => {
    const valueUnderTest = checkValidPassword("Abc123dshsdfjf")

    expect(valueUnderTest).toBe(true)
  })

  it("should return false for an invalid password", () => {
    const valueUnderTest = checkValidPassword("aaaaaaaaaaaa")

    expect(valueUnderTest).toBe(false)
  })

  it("should return false for an invalid password 2", () => {
    const valueUnderTest = checkValidPassword("Aaaaaaaaaaaa")

    expect(valueUnderTest).toBe(false)
  })
})
