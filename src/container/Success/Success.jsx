import React, { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"

import BackButton from "../../components/UIElements/BackButton/BackButton"
import Container from "../../components/UIElements/Container/Container"

import { TAB_ID } from "../../components/TabbedStatusBar/config/config"
import { SET_CURRENT_PAGE } from "../../store/actions/config/actions"

import tickSVG from "../../assets/icons/tick.svg"
import classes from "./Success.module.scss"

const Success = () => {
  const userProfileObject = useSelector(({ user }) => user)
  const privacyPreferencesObject = useSelector(
    ({ communications }) => communications
  )

  const dispatch = useDispatch()

  const mergedObject = { ...userProfileObject, ...privacyPreferencesObject }

  console.log("Complete form object: ", mergedObject)

  useEffect(() => {
    dispatch({ type: SET_CURRENT_PAGE, currentPage: TAB_ID.DONE })

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <Container>
      <BackButton to="comms" />
      <div className={classes.Success__Content}>
        <img src={tickSVG} alt="tick" />
        <h2 className={classes.Success__Verify}>
          Please verify your email address, you should have received an email
          from us already!
        </h2>
      </div>
    </Container>
  )
}

export default Success
