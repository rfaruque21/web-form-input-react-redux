import React from "react"
import configureStore from "redux-mock-store"
import { reduxRender as render } from "../../utils/test-utils"

import Success from "./Success"

import { TAB_ID } from "../../components/TabbedStatusBar/config/config"

const mockStore = configureStore([])

describe("Success", () => {
  it("should render the component and fire off the correct current page event", () => {
    const store = mockStore({ config: { currentPage: "" } })

    const { asFragment } = render(<Success />, { store })

    expect(store.getActions()).toStrictEqual([
      {
        currentPage: TAB_ID.DONE,
        type: "SET_CURRENT_PAGE"
      }
    ])

    expect(asFragment()).toMatchSnapshot()
  })
})
