import React from "react"
import configureStore from "redux-mock-store"

import { fireEvent, reduxRender as render } from "../../utils/test-utils"

import Communications from "./Communications"

import { TAB_ID } from "../../components/TabbedStatusBar/config/config"

const mockStore = configureStore([])

describe("Communications", () => {
  it("should render the component and fire off the correct current page event", () => {
    const store = mockStore({ config: { currentPage: "" } })

    const { asFragment } = render(<Communications />, { store })

    expect(store.getActions()).toStrictEqual([
      {
        currentPage: TAB_ID.PRIVACY,
        type: "SET_CURRENT_PAGE"
      }
    ])

    expect(asFragment()).toMatchSnapshot()
  })

  it("should render the correct state to the store when the receive update checbox is clicked", () => {
    const store = mockStore({
      communications: {
        receiveUpdates: false,
        receiveUpdateOtherProducts: false
      }
    })

    const { getByTestId } = render(<Communications />, { store })

    fireEvent.click(
      getByTestId("checkbox--Receive updates about Tray.io product by email")
    )

    expect(store.getActions()).toStrictEqual([
      { currentPage: "PRIVACY", type: "SET_CURRENT_PAGE" },
      {
        receiveUpdates: true,
        type: "SET_RECEIVE_UPDATES"
      }
    ])
  })
})
