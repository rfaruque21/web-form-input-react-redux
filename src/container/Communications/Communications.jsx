import React, { useEffect } from "react"
import { useDispatch } from "react-redux"
import { useHistory } from "react-router-dom"

import Container from "../../components/UIElements/Container/Container"
import Checkbox from "../../components/UIElements/Form/CheckboxWithLabel/CheckboxWithLabel"
import Button from "../../components/UIElements/Form/Button/Button"

import { TAB_ID } from "../../components/TabbedStatusBar/config/config"
import { SET_CURRENT_PAGE } from "../../store/actions/config/actions"
import {
  SET_RECEIVE_UPDATES,
  SET_RECEIVE_UPDATES_OTHER_PRODUCTS
} from "../../store/actions/communications/actions"

import classes from "./Communications.module.scss"
import BackButton from "../../components/UIElements/BackButton/BackButton"

const Communications = () => {
  const history = useHistory()
  const dispatch = useDispatch()

  const receiveUpdatesOnCheckedHandler = (check) => {
    dispatch({ type: SET_RECEIVE_UPDATES, receiveUpdates: check })
  }

  const receiveUpdatesOtherProductsOnCheckedHandler = (check) => {
    dispatch({
      type: SET_RECEIVE_UPDATES_OTHER_PRODUCTS,
      receiveUpdateOtherProducts: check
    })
  }

  const onClickHandler = () => {
    history.push("/success")
  }

  useEffect(() => {
    dispatch({ type: SET_CURRENT_PAGE, currentPage: TAB_ID.PRIVACY })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <Container>
      <BackButton to="/" />
      <Checkbox
        label="Receive updates about Tray.io product by email"
        onChecked={receiveUpdatesOnCheckedHandler}
      />

      <Checkbox
        label="Receive communication by email for other products created by the tray.io team"
        onChecked={receiveUpdatesOtherProductsOnCheckedHandler}
      />

      <div className={classes.Communications__CTA}>
        <Button onClick={onClickHandler} />
      </div>
    </Container>
  )
}

export default Communications
