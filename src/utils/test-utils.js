import React from "react"
import { render } from "@testing-library/react"
import { Router } from "react-router-dom"
import { Provider } from "react-redux"
import configureStore from "redux-mock-store"
import thunk from "redux-thunk"
import { createMemoryHistory } from "history"

import { initialState } from "../store"

const customRender = (
  ui,
  {
    store = configureStore([thunk], initialState),
    history = createMemoryHistory(),
    ...renderOptions
  } = {}
) => {
  const Wrapper = ({ children }) => {
    return (
      <Provider store={store}>
        <Router history={history}>{children}</Router>
      </Provider>
    )
  }
  return render(ui, { wrapper: Wrapper, ...renderOptions })
}

export * from "@testing-library/react"

export { customRender as reduxRender }
