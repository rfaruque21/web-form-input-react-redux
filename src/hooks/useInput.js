import { useState } from "react"
import PropTypes from "prop-types"

const useInput = (validator) => {
  const [value, setValue] = useState("")
  const [isTouched, setIsTouched] = useState(false)

  const isValid = validator(value)
  const error = !isValid && isTouched

  const onChangeHandler = (value) => setValue(value)
  const onBlurHandler = () => setIsTouched(true)

  return {
    value,
    onChangeHandler,
    onBlurHandler,
    isValid,
    error
  }
}

export default useInput

useInput.propTypes = {
  validator: PropTypes.func
}
