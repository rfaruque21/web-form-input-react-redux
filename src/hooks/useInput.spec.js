import { renderHook, act } from "@testing-library/react-hooks"

import useInput from "./useInput"

import { notEmpty } from "../container/UserInput/config/validators"

describe("useInput", () => {
  it("should update the value when the onChangeHandler is invoked", () => {
    const { result } = renderHook(() => useInput(notEmpty))

    expect(result.current.value).toBe("")
    expect(result.current.isValid).toBe(false)
    expect(result.current.error).toBe(false)

    act(() => {
      result.current.onChangeHandler("hello")
    })

    expect(result.current.value).toBe("hello")
    expect(result.current.isValid).toBe(true)
    expect(result.current.error).toBe(false)
  })

  it("should invoke an error if the conditions for it are correct (onblur triggered)", () => {
    const { result } = renderHook(() => useInput(notEmpty))

    expect(result.current.value).toBe("")
    expect(result.current.isValid).toBe(false)
    expect(result.current.error).toBe(false)

    act(() => {
      result.current.onBlurHandler(true)
      result.current.onChangeHandler("")
    })

    expect(result.current.value).toBe("")
    expect(result.current.isValid).toBe(false)
    expect(result.current.error).toBe(true)
  })

  it("should be able to recover from an error if the valid conditions are then subsequently met", () => {
    const { result } = renderHook(() => useInput(notEmpty))

    expect(result.current.value).toBe("")
    expect(result.current.isValid).toBe(false)
    expect(result.current.error).toBe(false)

    act(() => {
      result.current.onBlurHandler(true)
      result.current.onChangeHandler("")
    })

    expect(result.current.value).toBe("")
    expect(result.current.isValid).toBe(false)
    expect(result.current.error).toBe(true)

    act(() => {
      result.current.onChangeHandler("I am now valid")
    })

    expect(result.current.value).toBe("I am now valid")
    expect(result.current.isValid).toBe(true)
    expect(result.current.error).toBe(false)
  })
})
