import { Switch, Route, Redirect } from "react-router-dom"

import TabbedStatusBar from "./components/TabbedStatusBar/TabbedStatusBar"
import UserInput from "./container/UserInput/UserInput"
import Communications from "./container/Communications/Communications"
import Success from "./container/Success/Success"

const App = () => {
  return (
    <>
      <TabbedStatusBar />
      <Switch>
        <Route exact path="/">
          <UserInput />
        </Route>
        <Route exact path="/comms">
          <Communications />
        </Route>
        <Route exact path="/success">
          <Success />
        </Route>
        <Redirect to="/" />
      </Switch>
    </>
  )
}

export default App
