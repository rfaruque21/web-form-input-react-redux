import React from "react"
import PropTypes from "prop-types"
import classnames from "classnames"

import classes from "./Tab.module.scss"

const Tab = ({ id, displayName, selectedResource }) => {
  const styles = classnames(classes.Tab__Container, {
    [classes.Tab__Selected]: id === selectedResource,
  })

  return (
    <div data-testid={`tab--${id}--${displayName}`} className={styles}>
      {displayName}
    </div>
  )
}

export default Tab

Tab.propTypes = {
  id: PropTypes.string,
  displayName: PropTypes.string,
  selectedResource: PropTypes.string,
}
