import React from "react"
import { render } from "@testing-library/react"

import Tab from "./Tab"

describe("Tab", () => {
  it("should render the component", () => {
    const { asFragment, getByTestId } = render(
      <Tab id="test" displayName="displayName" selectedResource="test" />
    )

    expect(getByTestId("tab--test--displayName").textContent).toBe(
      "displayName"
    )
    expect(asFragment()).toMatchSnapshot()
  })

  it("should show the currently selected resource as selected", () => {
    const { getByTestId } = render(
      <Tab id="test" displayName="displayName" selectedResource="test" />
    )

    expect(getByTestId("tab--test--displayName")).toHaveClass(
      "Tab__Container Tab__Selected"
    )
  })

  it("should not show the currently selected resource as selected if it is indeed not", () => {
    const { getByTestId } = render(
      <Tab
        id="test"
        displayName="displayName"
        selectedResource="testNotSelected"
      />
    )

    expect(getByTestId("tab--test--displayName")).not.toHaveClass(
      "Tab__Container Tab__Selected"
    )
  })
})
