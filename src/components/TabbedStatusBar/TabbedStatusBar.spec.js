import React from "react"
import configureStore from "redux-mock-store"

import { reduxRender as render } from "../../utils/test-utils"
import { TAB_ID } from "./config/config"

import TabbedStatusBar from "./TabbedStatusBar"

const mockStore = configureStore([])

describe("TabbedStatusBar", () => {
  it("should render the component", () => {
    const store = mockStore({ config: { currentPage: TAB_ID.USER } })

    const { asFragment } = render(<TabbedStatusBar />, { store })

    expect(asFragment()).toMatchSnapshot()
  })

  it("should show the current page as selected only (user)", () => {
    const store = mockStore({ config: { currentPage: TAB_ID.USER } })

    const { getByTestId } = render(<TabbedStatusBar />, { store })

    expect(getByTestId(`tab--${TAB_ID.USER}--User`)).toHaveClass(
      "Tab__Selected"
    )
    expect(getByTestId(`tab--${TAB_ID.PRIVACY}--Privacy`)).not.toHaveClass(
      "Tab__Selected"
    )
    expect(getByTestId(`tab--${TAB_ID.DONE}--Done`)).not.toHaveClass(
      "Tab__Selected"
    )
  })

  it("should show the current page as selected only (privacy)", () => {
    const store = mockStore({ config: { currentPage: TAB_ID.PRIVACY } })

    const { getByTestId } = render(<TabbedStatusBar />, { store })

    expect(getByTestId(`tab--${TAB_ID.USER}--User`)).not.toHaveClass(
      "Tab__Container Tab__Selected"
    )
    expect(getByTestId(`tab--${TAB_ID.PRIVACY}--Privacy`)).toHaveClass(
      "Tab__Container Tab__Selected"
    )
    expect(getByTestId(`tab--${TAB_ID.DONE}--Done`)).not.toHaveClass(
      "Tab__Container Tab__Selected"
    )
  })

  it("should show the current page as selected only (done)", () => {
    const store = mockStore({ config: { currentPage: TAB_ID.DONE } })

    const { getByTestId } = render(<TabbedStatusBar />, { store })

    expect(getByTestId(`tab--${TAB_ID.USER}--User`)).not.toHaveClass(
      "Tab__Container Tab__Selected"
    )
    expect(getByTestId(`tab--${TAB_ID.PRIVACY}--Privacy`)).not.toHaveClass(
      "Tab__Container Tab__Selected"
    )
    expect(getByTestId(`tab--${TAB_ID.DONE}--Done`)).toHaveClass(
      "Tab__Container Tab__Selected"
    )
  })
})
