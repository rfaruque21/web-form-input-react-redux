const TAB_ID = {
  USER: "USER",
  PRIVACY: "PRIVACY",
  DONE: "DONE",
}

const TAB_CONSTANTS = [
  {
    id: TAB_ID.USER,
    displayName: "User",
  },
  {
    id: TAB_ID.PRIVACY,
    displayName: "Privacy",
  },
  {
    id: TAB_ID.DONE,
    displayName: "Done",
  },
]

export { TAB_CONSTANTS, TAB_ID }
