import React from "react"
import { useSelector } from "react-redux"

import Tab from "./components/Tab/Tab"

import { TAB_CONSTANTS as tabs } from "../../components/TabbedStatusBar/config/config"

import classes from "./TabbedStatusBar.module.scss"

const TabbedStatusBar = () => {
  const { currentPage } = useSelector(({ config }) => config)

  return (
    <div className={classes.TabbedStatusBar__Container}>
      {tabs.map(({ id, displayName }, index) => (
        <Tab
          key={index}
          id={id}
          displayName={displayName}
          selectedResource={currentPage}
        />
      ))}
    </div>
  )
}

export default TabbedStatusBar
