import React from "react"
import PropTypes from "prop-types"

import classes from "./Container.module.scss"

const Container = ({ children }) => {
  return <div className={classes.Container__Container}>{children}</div>
}

export default Container
Container.propTypes = {
  children: PropTypes.node
}
