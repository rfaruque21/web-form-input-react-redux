import React from "react"
import { render } from "@testing-library/react"

import Container from "./Container"

describe("Container", () => {
  it("should render the component", () => {
    const { asFragment } = render(<Container />)

    expect(asFragment()).toMatchSnapshot()
  })
})
