import React from "react"
import { Link } from "react-router-dom"
import PropTypes from "prop-types"

import backSVG from "../../../assets/icons/back.svg"
import classes from "./BackButton.module.scss"

const BackButton = ({ to }) => {
  return (
    <Link className={classes.BackButton__Container} to={to}>
      <img src={backSVG} alt="back"></img> <span aria-label="back">Back</span>
    </Link>
  )
}

export default BackButton

BackButton.propTypes = {
  to: PropTypes.string
}
