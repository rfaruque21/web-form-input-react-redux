import React from "react"
import configureStore from "redux-mock-store"
import { reduxRender as render } from "../../../utils/test-utils"

import BackButton from "./BackButton"

const mockStore = configureStore([])

describe("BackButton", () => {
  it("should render the component", () => {
    const store = mockStore({ user: {}, communications: {}, config: {} })

    const { asFragment } = render(<BackButton to="/" />, { store })

    expect(asFragment()).toMatchSnapshot()
  })
})
