import React from "react"
import PropTypes from "prop-types"

import classes from "./Button.module.scss"

const Button = ({ id, disable, onClick, buttonText = "Submit" }) => {
  return (
    <button
      data-testid={`button--${id}`}
      aria-label={buttonText}
      className={classes.Button}
      onClick={onClick}
      disabled={disable}
    >
      {buttonText}
    </button>
  )
}

export default Button

Button.propTypes = {
  id: PropTypes.string,
  disable: PropTypes.bool,
  onClick: PropTypes.func,
  buttonText: PropTypes.string
}
