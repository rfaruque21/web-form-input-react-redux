import React from "react"
import { render, fireEvent } from "@testing-library/react"

import Button from "./Button"

describe("Button", () => {
  it("should render the button", () => {
    const { asFragment } = render(<Button id="test" />)

    expect(asFragment()).toMatchSnapshot()
  })

  it("should send an event via the on click handler when pressed", () => {
    const mockOnClickHandler = jest.fn()

    const { getByTestId } = render(
      <Button id="test" onClick={mockOnClickHandler} />
    )

    fireEvent.click(getByTestId("button--test"))

    expect(mockOnClickHandler).toBeCalled()
  })

  it("should disable the button when the disable prop is set", () => {
    const mockOnClickHandler = jest.fn()

    const { getByTestId } = render(
      <Button id="test" onClick={mockOnClickHandler} disable={true} />
    )

    fireEvent.click(getByTestId("button--test"))

    expect(getByTestId("button--test")).toHaveAttribute("disabled")

    expect(mockOnClickHandler).not.toBeCalled()
  })

  it("should reflect the button label text when the button label prop is set", () => {
    const { getByTestId } = render(<Button id="test" buttonText="buttonText" />)

    expect(getByTestId("button--test").textContent).toBe("buttonText")
  })
})
