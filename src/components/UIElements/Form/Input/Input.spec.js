import React from "react"
import { render, fireEvent, waitFor } from "@testing-library/react"

import Input from "./Input"

describe("Input", () => {
  it("should render the component", () => {
    const { asFragment, getByTestId } = render(
      <Input id="test" labelText="labelText" />
    )

    expect(getByTestId("input--label--test").textContent).toBe("labelText:")
    expect(asFragment()).toMatchSnapshot()
  })

  it("should fire off the relevant events when invoked", () => {
    const mockOnChangeHandler = jest.fn()
    const mockOnBlurHandler = jest.fn()

    const { getByTestId } = render(
      <Input
        id="test"
        onChange={mockOnChangeHandler}
        onBlur={mockOnBlurHandler}
      />
    )

    fireEvent.change(getByTestId("input--test"), { target: { value: "sdfd" } })

    expect(mockOnChangeHandler).toBeCalled()

    fireEvent.blur(getByTestId("input--test"))

    expect(mockOnBlurHandler).toBeCalled()
  })

  it("should show the error message when error is set to true", () => {
    const { getByTestId, queryByTestId } = render(
      <Input id="test" errorText="I am some error text" error={true} />
    )

    expect(queryByTestId("input--error--test")).toBeTruthy()
    expect(getByTestId("input--error--test").textContent).toBe(
      "I am some error text"
    )
  })

  it("should show the required pseudo if set", async () => {
    const { asFragment, getByTestId } = render(
      <Input id="test" labelText="labelText" required={true} />
    )

    waitFor(() =>
      expect(getByTestId("input--label--test")).toHaveClass("Required")
    )
    expect(asFragment()).toMatchSnapshot()
  })
})
