import React from "react"
import { render } from "@testing-library/react"

import LabelHeader from "./LabelHeader"

describe("LabelHeader", () => {
  it("should render the component", () => {
    const { asFragment, getByTestId } = render(
      <LabelHeader labelText="I am label text" id="test" />
    )

    expect(getByTestId("input--label--test").textContent).toBe(
      "I am label text:"
    )

    expect(asFragment()).toMatchSnapshot()
  })
})
