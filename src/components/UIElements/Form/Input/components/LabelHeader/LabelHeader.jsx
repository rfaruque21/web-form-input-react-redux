import React from "react"
import PropTypes from "prop-types"

const LabelHeader = ({ id, labelText }) => (
  <label data-testid={`input--label--${id}`} htmlFor={labelText}>
    {labelText}:
  </label>
)

export default LabelHeader

LabelHeader.propTypes = {
  id: PropTypes.string,
  labelText: PropTypes.string
}
