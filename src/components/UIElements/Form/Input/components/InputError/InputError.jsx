import React from "react"
import PropTypes from "prop-types"

import classes from "./InputError.module.scss"

const InputError = ({ show = false, errorText, id }) => (
  <>
    {show && (
      <h5
        data-testid={`input--error--${id}`}
        className={classes.InputError__Container}
      >
        {errorText}
      </h5>
    )}
  </>
)

export default InputError

InputError.propTypes = {
  show: PropTypes.bool,
  errorText: PropTypes.string,
  id: PropTypes.string
}
