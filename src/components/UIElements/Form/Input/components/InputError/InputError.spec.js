import React from "react"
import { render } from "@testing-library/react"

import InputError from "./InputError"

describe("InputError", () => {
  it("should render the component", () => {
    const { asFragment, getByTestId } = render(
      <InputError show={true} errorText="I am error text" id="test" />
    )

    expect(getByTestId("input--error--test").textContent).toBe(
      "I am error text"
    )

    expect(asFragment()).toMatchSnapshot()
  })
})
