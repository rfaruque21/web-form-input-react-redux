import React from "react"
import PropTypes from "prop-types"
import classnames from "classnames"

import LabelHeader from "./components/LabelHeader/LabelHeader"
import Error from "./components/InputError/InputError"

import classes from "./Input.module.scss"

const Input = ({
  id,
  value,
  required,
  labelText,
  onChange,
  onBlur,
  errorText,
  error,
  type = "text"
}) => {
  const containerStyles = classnames(classes.Input__Container, {
    [classes.Input__Container_Required]: required
  })

  return (
    <div className={containerStyles}>
      <LabelHeader id={id} labelText={labelText} required={required} />
      <input
        data-testid={`input--${id}`}
        aria-label={labelText}
        aria-required={required}
        id={labelText}
        type={type}
        onChange={({ target: { value } }) => onChange(value)}
        onBlur={onBlur}
        value={value}
      />
      <Error show={error} errorText={errorText} id={id} />
    </div>
  )
}

export default Input

Input.propTypes = {
  id: PropTypes.string,
  required: PropTypes.bool,
  labelText: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  errorText: PropTypes.string,
  error: PropTypes.bool,
  value: PropTypes.any,
  type: PropTypes.oneOf(["text", "password"])
}
