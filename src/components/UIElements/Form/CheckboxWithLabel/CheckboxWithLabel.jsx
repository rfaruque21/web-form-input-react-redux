import React from "react"
import PropTypes from "prop-types"

const CheckboxWithLabel = ({ onChecked, label }) => {
  return (
    <div>
      <input
        data-testid={`checkbox--${label}`}
        id={label}
        type="checkbox"
        onChange={({ target: { checked } }) => onChecked(checked)}
      />
      <label
        data-testid={`checkbox--${label}--label`}
        aria-label={label}
        htmlFor={label}
      >
        {label}
      </label>
    </div>
  )
}

export default CheckboxWithLabel

CheckboxWithLabel.propTypes = {
  onChecked: PropTypes.func,
  label: PropTypes.string.isRequired
}
