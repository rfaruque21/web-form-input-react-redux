import React from "react"
import { fireEvent, render } from "@testing-library/react"

import CheckboxWithLabel from "./CheckboxWithLabel"

describe("CheckboxWithLabel", () => {
  it("should render the component", () => {
    const { asFragment, getByTestId } = render(
      <CheckboxWithLabel label="test" />
    )

    expect(getByTestId("checkbox--test--label").textContent).toBe("test")
    expect(asFragment()).toMatchSnapshot()
  })

  it("should fire off the onChecked event when the checkbox checked status is changed", () => {
    const mockOnCheckedHandler = jest.fn()

    const { getByTestId } = render(
      <CheckboxWithLabel onChecked={mockOnCheckedHandler} label="test" />
    )

    fireEvent.click(getByTestId("checkbox--test"))

    expect(mockOnCheckedHandler).toBeCalled()
  })
})
