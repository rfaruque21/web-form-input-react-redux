import {
  SET_EMAIL,
  SET_NAME,
  SET_PASSWORD,
  SET_ROLE
} from "../../actions/user/actions"

export const initialState = {
  name: "",
  role: "",
  email: "",
  password: ""
}

const reducer = (
  state = initialState,
  { type, name, role, email, password }
) => {
  switch (type) {
    case SET_NAME:
      return { ...state, name }

    case SET_ROLE:
      return {
        ...state,
        role
      }

    case SET_EMAIL:
      return { ...state, email }

    case SET_PASSWORD:
      return { ...state, password }

    default:
      return state
  }
}

export default reducer
