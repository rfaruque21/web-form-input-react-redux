import userReducer from "./user"

import {
  SET_EMAIL,
  SET_NAME,
  SET_PASSWORD,
  SET_ROLE
} from "../../actions/user/actions"

const initialState = {
  name: "",
  role: "",
  email: "",
  password: ""
}

describe("user reducer", () => {
  it("should set the name", () => {
    const returnedState = userReducer(initialState, {
      type: SET_NAME,
      name: "name",
      role: "",
      email: "",
      password: ""
    })

    expect(returnedState.name).toBe("name")
    expect(returnedState.role).toBe("")
    expect(returnedState.email).toBe("")
    expect(returnedState.password).toBe("")
  })

  it("should set the role", () => {
    const returnedState = userReducer(initialState, {
      type: SET_ROLE,
      name: "",
      role: "role",
      email: "",
      password: ""
    })

    expect(returnedState.name).toBe("")
    expect(returnedState.role).toBe("role")
    expect(returnedState.email).toBe("")
    expect(returnedState.password).toBe("")
  })

  it("should set the email", () => {
    const returnedState = userReducer(initialState, {
      type: SET_EMAIL,
      name: "",
      role: "",
      email: "email",
      password: ""
    })

    expect(returnedState.name).toBe("")
    expect(returnedState.role).toBe("")
    expect(returnedState.email).toBe("email")
    expect(returnedState.password).toBe("")
  })

  it("should set the password", () => {
    const returnedState = userReducer(initialState, {
      type: SET_PASSWORD,
      name: "",
      role: "",
      email: "",
      password: "password"
    })

    expect(returnedState.name).toBe("")
    expect(returnedState.role).toBe("")
    expect(returnedState.email).toBe("")
    expect(returnedState.password).toBe("password")
  })
})
