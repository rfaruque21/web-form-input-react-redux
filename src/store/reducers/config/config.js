import { TAB_ID } from "../../../components/TabbedStatusBar/config/config"
import { SET_CURRENT_PAGE } from "../../actions/config/actions"

export const initialState = {
  currentPage: TAB_ID.USER,
}

const reducer = (state = initialState, { type, currentPage }) => {
  switch (type) {
    case SET_CURRENT_PAGE:
      return { ...state, currentPage }

    default:
      return state
  }
}

export default reducer
