import configReducer from "./config"

import { SET_CURRENT_PAGE } from "../../actions/config/actions"

const initialState = {
  currentPage: "",
}

describe("config reducer", () => {
  it("should modify the correct state for current page", () => {
    const returnedState = configReducer(initialState, {
      type: SET_CURRENT_PAGE,
      currentPage: "current",
    })

    expect(returnedState.currentPage).toBe("current")
  })
})
