import communicationsReducer from "./communications"

import {
  SET_RECEIVE_UPDATES,
  SET_RECEIVE_UPDATES_OTHER_PRODUCTS,
} from "../../actions/communications/actions"

const initialState = {
  receiveUpdates: false,
  receiveUpdateOtherProducts: false,
}

describe("communications reducer", () => {
  it("should modify the correct state for receive updates", () => {
    const returnedState = communicationsReducer(initialState, {
      type: SET_RECEIVE_UPDATES,
      receiveUpdates: true,
      receiveUpdateOtherProducts: false,
    })

    expect(returnedState.receiveUpdates).toBe(true)
    expect(returnedState.receiveUpdateOtherProducts).toBe(false)
  })

  it("should modify the correct state for receive updates other products", () => {
    const returnedState = communicationsReducer(initialState, {
      type: SET_RECEIVE_UPDATES_OTHER_PRODUCTS,
      receiveUpdates: false,
      receiveUpdateOtherProducts: true,
    })

    expect(returnedState.receiveUpdates).toBe(false)
    expect(returnedState.receiveUpdateOtherProducts).toBe(true)
  })
})
