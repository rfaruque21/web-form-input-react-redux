import {
  SET_RECEIVE_UPDATES,
  SET_RECEIVE_UPDATES_OTHER_PRODUCTS
} from "../../actions/communications/actions"

export const initialState = {
  receiveUpdates: false,
  receiveUpdateOtherProducts: false
}

const reducer = (
  state = initialState,
  { type, receiveUpdates, receiveUpdateOtherProducts }
) => {
  switch (type) {
    case SET_RECEIVE_UPDATES:
      return { ...state, receiveUpdates }

    case SET_RECEIVE_UPDATES_OTHER_PRODUCTS:
      return {
        ...state,
        receiveUpdateOtherProducts
      }

    default:
      return state
  }
}

export default reducer
