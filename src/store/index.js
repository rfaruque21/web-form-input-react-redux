import { combineReducers } from "redux"

import user, { initialState as userInitialState } from "./reducers/user/user"
import communications, {
  initialState as communicationsInitialState
} from "./reducers/communications/communications"
import config, {
  initialState as configInitialState
} from "./reducers/config/config"

const combinedReducers = combineReducers({ user, communications, config })

export const initialState = {
  user: userInitialState,
  communications: communicationsInitialState,
  config: configInitialState
}

export default combinedReducers
