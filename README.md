# Getting Started

If you'd like to run the project locally, please run `npm install` in the root directory and then follow the next script

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.

## Inspecting the redux store

You can download this redux chrome extension https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd to view the state of the Redux Store.

## Miscellaneous

I'm aware that there are 3rd party libraries available to deal with Form inputs such as Formik but I wasn't sure if this would be classed as something that would directly address the problem domain so I created my own version instead.
